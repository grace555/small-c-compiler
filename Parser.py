from Lexer import Lexer

class Node:
    def __init__(self, kind, option1=None, option2=None, option3=None, value=None):
        self.kind = kind
        self.value = value
        self.option1 = option1
        self.option2 = option2
        self.option3 = option3


class Parser:

    VAR, CONST, STR, PRINT, ADD, SUB, LESS,  EQLESS, EQUAL,  SET, IF1, IF2, WHILE, DO, EMPTY, SEQ, EXPR, PROG = range(18)

    def __init__(self, lexer):
        self.lexer = lexer

    def error(self, msg):
        print msg
        exit()


    def term(self):
        if self.lexer.symbol == Lexer.ID:
            n = Node(Parser.VAR, value=self.lexer.value)
            self.lexer.next_tok()
            return n
        if self.lexer.symbol == Lexer.NUM:
            n = Node(Parser.CONST, value=self.lexer.value )
            self.lexer.next_tok()
            return n
        return self.parenthesis_expression()

    def summa(self):
        node = self.term()
        while self.lexer.symbol == Lexer.PLUS or self.lexer.symbol == Lexer.MINUS:
            if self.lexer.symbol == Lexer.PLUS:
                kind = Parser.ADD
            else:
                kind = Parser.SUB
            self.lexer.next_tok()
            node = Node(kind, node, self.term())
        return node

    def test(self):
        node = self.summa()
        if self.lexer.symbol == Lexer.LESS:
            self.lexer.next_tok()
            node = Node(Parser.LESS, node, self.summa())
        elif self.lexer.symbol == Lexer.MORE:
            self.lexer.next_tok()
            node = Node(Parser.LESS, self.summa(), node)
        elif self.lexer.symbol == Lexer.EQUAL:
            self.lexer.next_tok()
            node = Node(Parser.EQUAL, node, self.summa())
        elif self.lexer.symbol == Lexer.EQLESS:
            self.lexer.next_tok()
            node = Node(Parser.EQLESS, node, self.summa())
        elif self.lexer.symbol == Lexer.EQMORE:
            self.lexer.next_tok()
            node = Node(Parser.EQLESS, self.summa(), node)
        return node

    def expression(self):
        if not self.lexer.symbol == Lexer.ID:
            return self.test()
        node = self.test()
        if node.kind == Parser.VAR and self.lexer.symbol == Lexer.SETVAL:
            self.lexer.next_tok()
            node = Node(Parser.SET, node, self.expression())
        return node

    def create_Node(self, kind):
        node = Node(kind)
        self.lexer.next_tok()
        return node

    def statement(self):
        if self.lexer.symbol == Lexer.IF:
            n =  self.create_Node(Parser.IF1)
            n.option1 = self.parenthesis_expression()
            n.option2 = self.statement()
            if self.lexer.symbol == Lexer.ELSE:
                n.kind = Parser.IF2
                self.lexer.next_tok()
                n.option3 = self.statement()
        elif self.lexer.symbol == Lexer.WHILE:
            n = self.create_Node(Parser.WHILE)
            n.option1 = self.parenthesis_expression()
            n.option2 = self.statement()
        elif self.lexer.symbol == Lexer.DO:
            n = self.create_Node(Parser.DO)
            n.option1 = self.statement()
            if not self.lexer.symbol == Lexer.WHILE:
                self.error('WHILE expected')
            self.lexer.next_tok()
            n.option2 = self.parenthesis_expression()
            if not self.lexer.symbol == Lexer.SEMICOLON:
                self.error('; expected')
        elif self.lexer.symbol == Lexer.SEMICOLON:
            n = Node(Parser.EMPTY)
            self.lexer.next_tok()
        elif self.lexer.symbol == Lexer.LBRA:
            n = Node(Parser.EMPTY)
            self.lexer.next_tok()
            while not self.lexer.symbol == Lexer.RBRA:
                n = Node(Parser.SEQ, n, self.statement())
            self.lexer.next_tok()
        elif self.lexer.symbol == Lexer.PRINT:
            n = self.create_Node(Parser.PRINT)
            n.option1 = self.print_parenthesis_expr()
        else:
            n = Node(Parser.EXPR, self.expression())
            if not self.lexer.symbol == Lexer.SEMICOLON:
                self.error('; expected')
            self.lexer.next_tok()
        return n


    def parse(self):
        self.lexer.next_tok()
        node = Node(self.PROG, self.statement())
        if not self.lexer.symbol == Lexer.EOF:
            self.error("Invalid statement syntax")
        return node


    def parenthesis_expression(self):
        if not self.lexer.symbol == Lexer.LPAR:
            self.error('( expexted')
        self.lexer.next_tok()
        node = self.expression()
        if not self.lexer.symbol == Lexer.RPAR:
            self.error(') expexted')
        self.lexer.next_tok()
        return node

    def print_parenthesis_expr(self):
        if not self.lexer.symbol == Lexer.LPAR:
            self.error('( expexted')
        self.lexer.next_tok()
        node = self.print_expression()
        if not self.lexer.symbol == Lexer.RPAR:
            self.error(') expexted')
        self.lexer.next_tok()
        return node

    def print_expression(self):
        node = self.print_term()
        if not node.kind == Parser.STR:
            while self.lexer.symbol == Lexer.PLUS or self.lexer.symbol == Lexer.MINUS:
                if self.lexer.symbol == Lexer.PLUS:
                    kind = Parser.ADD
                else:
                    kind = Parser.SUB
                self.lexer.next_tok()
                node = Node(kind, node, self.print_term())
        return node

    def print_term(self):
        if self.lexer.symbol == Lexer.ID:
            n = Node(Parser.VAR, value=self.lexer.value)
            self.lexer.next_tok()
            return n
        if self.lexer.symbol == Lexer.NUM:
            n = Node(Parser.CONST, value=self.lexer.value)
            self.lexer.next_tok()
            return n
        if self.lexer.symbol == Lexer.STR:
            n = Node(Parser.STR, value=self.lexer.value)
            self.lexer.next_tok()
            return n
        self.error('Unexpected symbol')









