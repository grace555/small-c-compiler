class VirtualMachine:
    IFETCH, ISTORE, IPUSH, IPOP, IADD, ISUB, ILESS, IEQUAL, IQLESS, JZ, JNZ, JMP, PRINT, HALT = range(14)

    def run(self, program, lexer):
        var = [0 for i in range(lexer.index)]
        stack = []
        pointer = 0

        while True:
            operator = program[pointer]
            if pointer < len(program) - 1:
                argument = program[pointer + 1]

            if operator == self.IFETCH:
                stack.append(var[argument])
                pointer += 2
            elif operator ==self.ISTORE:
                var[argument] = stack.pop()
                pointer += 2
            elif operator == self.IPUSH:
                stack.append(argument)
                pointer += 2
            elif operator == self.IPOP:
                stack.append(argument)
                stack.pop()
                pointer += 1
            elif operator == self.IADD:
                stack[-2] += stack[-1]
                stack.pop()
                pointer += 1
            elif operator == self.ISUB:
                stack[-2] -= stack[-1]
                stack.pop()
                pointer += 1
            elif operator == self.ILESS:
                if stack[-2] < stack[-1]:
                    stack[-2] = 1
                else:
                    stack[-2] = 0
                stack.pop()
                pointer += 1
            elif operator == self.IQLESS:
                if stack[-2] <= stack[-1]:
                    stack[-2] = 1
                else:
                    stack[-2] = 0
                stack.pop()
                pointer += 1
            elif operator == self.IEQUAL:
                if stack[-2] == stack[-1]:
                    stack[-2] = 1
                else:
                    stack[-2] = 0
                stack.pop()
                pointer += 1
            elif operator == self.JZ:
                if stack.pop() == 0:
                    pointer = argument
                else:
                    pointer +=2
            elif operator == self.JNZ:
                if not stack.pop() == 0:
                    pointer = argument
                else:
                    pointer +=2
            elif operator == self.JMP:
                pointer = argument
            elif operator == self.PRINT:
                print (stack[-1])
                stack.pop()
                pointer +=1
            elif operator == self.HALT:
                break