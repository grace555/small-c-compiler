import os.path

class Lexer:

    NUM, ID, PRINT, TYPE, STR, IF, ELSE, WHILE, DO, LBRA, RBRA, LPAR, RPAR, PLUS, MINUS, LESS, EQUAL, SEMICOLON, EOF, MORE, EQMORE, EQLESS, SETVAL = range(23)

    SYMBOLS = {'{': LBRA, '}': RBRA, '=': SETVAL, ';': SEMICOLON, '(': LPAR,')': RPAR, '+': PLUS, '-': MINUS, '<': LESS, '>': MORE}

    WORDS = {'if': IF, 'else': ELSE, 'do': DO, 'while': WHILE,  'print': PRINT}

    LONG_SYMBOLS = {'==': EQUAL, '<=': EQLESS, '>=': EQMORE}

    def __init__(self, filename):
        if not os.path.isfile(filename):
            raise LookupError('File does not exist')
        f = open(filename)
        self.f = f
        self.ch = ' '
        self.value = None
        self.symbol = None
        self.variables = {}
        self.index = 0

    def error(self, message):
        raise Exception(message)

    def read_next(self):
        self.ch = self.f.read(1)

    def next_tok(self):
        self.symbol = None
        self.value = None
        while self.symbol is None:
            if self.ch == '':
                self.symbol = Lexer.EOF
            elif self.ch == '"':
                self.read_next()
                string = ''
                while not self.ch == '"':
                    string += self.ch
                    self.read_next()
                self.read_next()
                self.symbol = self.STR
                self.value = string
            elif self.ch.isspace():
                self.read_next()
            elif self.ch in Lexer.SYMBOLS:
                if self.ch in ['=', '<', '>']:
                    prev = self.ch
                    self.long_operator(prev)
                else:
                    self.symbol = Lexer.SYMBOLS[self.ch]
                    self.read_next()
            elif self.ch.isdigit():
                intvalue = 0
                while self.ch.isdigit():
                    intvalue = intvalue * 10 + int(self.ch)
                    self.read_next()
                if self.ch.isalpha():
                    raise ValueError('')
                self.symbol = Lexer.NUM
                self.value = intvalue
            elif self.ch.isalpha():
                name = ''
                while self.ch.isalpha() or self.ch.isdigit():
                    name += self.ch
                    self.read_next()
                if name in Lexer.WORDS:
                    self.symbol = Lexer.WORDS[name]
                else:
                    self.symbol = Lexer.ID
                    if name in self.variables.values():
                        for i in range(len(self.variables)):
                            if self.variables[i] == name:
                                self.value = i
                    else:
                        self.variables[self.index] = name
                        self.value = self.index
                        self.index += 1
            else:
                self.error('')


    def long_operator(self, chr):
        self.read_next()
        if self.ch == '=':
            self.symbol = self.LONG_SYMBOLS[chr + self.ch]
        else:
            self.symbol = self.SYMBOLS[chr]
        self.read_next()