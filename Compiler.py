from Parser import Parser, Node
from VirtualMachine import VirtualMachine

class Compiler:

    def __init__(self):
        self.program = []
        self.pointer = 0

    def generate(self, command):
        self.program.append(command)
        self.pointer += 1

    def compile(self, node):
        if node.kind == Parser.VAR:
            self.generate(VirtualMachine.IFETCH)
            self.generate(node.value)
        elif node.kind == Parser.CONST:
            self.generate(VirtualMachine.IPUSH)
            self.generate(node.value)
        elif node.kind == Parser.STR:
            self.generate(VirtualMachine.IPUSH)
            self.generate(node.value)
        elif node.kind == Parser.ADD:
            self.compile(node.option1)
            self.compile(node.option2)
            self.generate(VirtualMachine.IADD)
        elif node.kind == Parser.SUB:
            self.compile(node.option1)
            self.compile(node.option2)
            self.generate(VirtualMachine.ISUB)
        elif node.kind == Parser.LESS:
            self.compile(node.option1)
            self.compile(node.option2)
            self.generate(VirtualMachine.ILESS)
        elif node.kind == Parser.EQLESS:
            self.compile(node.option1)
            self.compile(node.option2)
            self.generate(VirtualMachine.IQLESS)
        elif node.kind == Parser.EQUAL:
            self.compile(node.option1)
            self.compile(node.option2)
            self.generate(VirtualMachine.IEQUAL)
        elif node.kind ==Parser.SET:
            self.compile(node.option2)
            self.generate(VirtualMachine.ISTORE)
            self.generate(node.option1.value)
        elif node.kind == Parser.IF1:
            self.compile(node.option1)
            self.generate(VirtualMachine.JZ)
            address = self.pointer
            self.generate(0)
            self.compile(node.option2)
            self.program[address] = self.pointer
        elif node.kind == Parser.IF2:
            self.compile(node.option1)
            self.generate(VirtualMachine.JZ)
            address1 = self.pointer
            self.generate(0)
            self.compile(node.option2)
            self.generate(VirtualMachine.JMP)
            address2 = self.pointer
            self.generate(0)
            self.program[address1] = self.pointer
            self.compile(node.option3)
            self.program[address2] = self.pointer
        elif node.kind == Parser.WHILE:
            address1 = self.pointer
            self.compile(node.option1)
            self.generate(VirtualMachine.JZ)
            address2 = self.pointer
            self.generate(0)
            self.compile(node.option2)
            self.generate(VirtualMachine.JMP)
            self.generate(address1)
            self.program[address2] = self.pointer
        elif node.kind == Parser.DO:
            address = self.pointer
            self.compile(node.option1)
            self.compile(node.option2)
            self.generate(VirtualMachine.JNZ)
            self.generate(address)
        elif node.kind == Parser.SEQ:
            self.compile(node.option1)
            self.compile(node.option2)
        elif node.kind == Parser.EXPR:
            self.compile(node.option1)
            self.generate(VirtualMachine.IPOP)
        elif node.kind == Parser.PRINT:
            self.compile(node.option1)
            self.generate(VirtualMachine.PRINT)
        elif node.kind == Parser.PROG:
            self.compile(node.option1)
            self.generate(VirtualMachine.HALT)
        return  self.program




