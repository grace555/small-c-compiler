from VirtualMachine import VirtualMachine
from Lexer import Lexer
from Compiler import Compiler, Node
from Parser import Parser
import sys, argparse, os.path




parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', help="file with data")
args = parser.parse_args()
filename = args.file

lexer = Lexer(filename)

parser = Parser(lexer)
tree = parser.parse()
compiler = Compiler()
program = compiler.compile(tree)
vm = VirtualMachine()
vm.run(program, lexer)


